package com.example.estherapp.DI;

import com.example.estherapp.UI.Register.RegisterActivity;
import com.example.estherapp.UI.Register.RegisterModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = RegisterModule.class)
    abstract RegisterActivity tasksActivity();

    //@ActivityScoped
    //@ContributesAndroidInjector(modules = MainModule.class)
    //abstract MainActivity mainActivity();

}
