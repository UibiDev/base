package com.example.estherapp.DI;

import android.app.Application;

import com.example.estherapp.AppEstructura;
import com.example.estherapp.Data.ModuleUser.Repository.RepositoryUser;
import com.example.estherapp.Data.RepositoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        RepositoryModule.class,
        ApplicationModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class}
)

public interface AppComponet extends AndroidInjector<DaggerApplication> {

    void inject(AppEstructura application);

    RepositoryUser getRepositoryUsuario();

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponet.Builder application(Application application);
        AppComponet build();

    }

}
