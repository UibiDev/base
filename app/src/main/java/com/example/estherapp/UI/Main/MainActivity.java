package com.example.estherapp.UI.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.estherapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
