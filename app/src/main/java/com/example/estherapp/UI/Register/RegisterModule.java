package com.example.estherapp.UI.Register;

import com.example.estherapp.DI.ActivityScoped;
import com.example.estherapp.DI.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RegisterModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract RegisterFragment tasksFragment();

    @ActivityScoped
    @Binds
    abstract RegisterContract.Presenter taskPresenter(RegisterPresenter presenter);
}
