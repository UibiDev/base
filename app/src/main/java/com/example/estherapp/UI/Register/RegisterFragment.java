package com.example.estherapp.UI.Register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.estherapp.DI.ActivityScoped;
import com.example.estherapp.Model.User;
import com.example.estherapp.R;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

@ActivityScoped
public class RegisterFragment extends DaggerFragment implements RegisterContract.View{

    private TextView userSaved;

    @Inject
    RegisterContract.Presenter mPresenter;

    @Inject
    public RegisterFragment() { }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_register, container, false);
        init(root);

        return root;

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.dropView();
    }

    private void init(View view){
        Button addRecord = view.findViewById(R.id.add_record);
        userSaved = view.findViewById(R.id.userSaved);

        addRecord.setOnClickListener(v -> {
            mPresenter.saveNewUser(
                    new User(
                            0,
                            "",
                            "",
                            "",
                            "",
                            "",
                            1
                    ));
        });
    }

    @Override
    public void viewID(long id) {
        userSaved.setText("" + id);
    }
}
