package com.example.estherapp.UI.Register;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.estherapp.DI.ActivityScoped;
import com.example.estherapp.R;
import com.example.estherapp.Util.ActivityUtils;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class RegisterActivity extends DaggerAppCompatActivity {

    @Inject
    RegisterPresenter mTasksPresenter;

    @Inject
    RegisterFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (registerFragment == null) {
            registerFragment = fragment;
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(),
                    registerFragment, R.id.contentFrame
            );
        }

    }
}
