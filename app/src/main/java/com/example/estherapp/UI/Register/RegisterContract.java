package com.example.estherapp.UI.Register;

import com.example.estherapp.BasePresenter;
import com.example.estherapp.BaseView;
import com.example.estherapp.Model.User;

public interface RegisterContract {
    interface View extends BaseView<Presenter> {
        void viewID(long id);
    }

    interface Presenter extends BasePresenter<View> {
        void saveNewUser(User user);
        void getUser();
    }
}
