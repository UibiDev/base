package com.example.estherapp.UI.Register;

import android.util.Log;

import com.example.estherapp.Data.ModuleUser.Repository.RepositoryUser;
import com.example.estherapp.Data.ModuleUser.UserDataSource;
import com.example.estherapp.Model.User;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class RegisterPresenter implements RegisterContract.Presenter {

    private final RepositoryUser Repository;

    @Nullable
    private RegisterContract.View View;

    @Inject
    RegisterPresenter(RepositoryUser tasksRepository) {
        Repository = tasksRepository;
    }

    @Override
    public void saveNewUser(User user) {
        Repository.saveUser(user, new UserDataSource.response() {
            @Override
            public void successful(long res) {
                if (View != null){
                    Log.i("Login", "" + res);
                    getUser();
                }
            }

            @Override
            public void fail(String message) {
                Log.i("Login",message);
            }
        });
    }

    @Override
    public void getUser(){

        Repository.getLast(new UserDataSource.GetLast() {
            @Override
            public void lastUser(User user) {
                if (View != null){
                    View.viewID(user.idUser);
                }
            }
        });

        /*
        Repository.getAllUser(new UserDataSource.selectRecords() {
            @Override
            public void records(List<User> users) {
                for (User user: users) {
                    Log.i("Login","From DB -> UserID: " + user.idUser);
                }
            }

            @Override
            public void message(String message) {
                Log.i("Login", "message " + message);
            }
        });
        */
    }

    @Override
    public void takeView(RegisterContract.View view) {
        this.View = view;
    }

    @Override
    public void dropView() {
        this.View = null;
    }
}
