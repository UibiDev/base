package com.example.estherapp;

public interface BasePresenter<T> {

    void takeView(T view);
    void dropView();

}
