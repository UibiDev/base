package com.example.estherapp;

import androidx.annotation.VisibleForTesting;

import com.example.estherapp.DI.AppComponet;
import com.example.estherapp.DI.DaggerAppComponet;
import com.example.estherapp.Data.ModuleUser.Repository.RepositoryUser;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class AppEstructura extends DaggerApplication {

    @Inject
    RepositoryUser repositoryUsuario;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponet appComponent = DaggerAppComponet.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    @VisibleForTesting
    public RepositoryUser getRepositoryUsuario(){
        return repositoryUsuario;
    }

}
