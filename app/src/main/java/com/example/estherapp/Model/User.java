package com.example.estherapp.Model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "User")
public class User {

    @PrimaryKey(autoGenerate = true)
    public final int idUser;

    @Nullable
    @ColumnInfo(name = "Nombre")
    public final String Nombre;

    @Nullable
    @ColumnInfo(name = "APaterno")
    public final String APaterno;

    @Nullable
    @ColumnInfo(name = "AMaterno")
    public final String AMaterno;

    @Nullable
    @ColumnInfo(name = "FechaNacimiento")
    public final String FechaNacimiento;

    @Nullable
    @ColumnInfo(name = "Correo")
    public final String Correo;

    @Nullable
    @ColumnInfo(name = "IdArea")
    public final int IdArea;

    public User(int idUser, @Nullable String Nombre, @Nullable String APaterno, @Nullable String AMaterno,
                @Nullable String FechaNacimiento, @Nullable String Correo, int IdArea) {
        this.idUser = idUser;
        this.Nombre = Nombre;
        this.APaterno = APaterno;
        this.AMaterno = AMaterno;
        this.FechaNacimiento = FechaNacimiento;
        this.Correo = Correo;
        this.IdArea = IdArea;
    }

    public int getIdUser() {
        return idUser;
    }

    @Nullable
    public String getNombre() {
        return Nombre;
    }

    @Nullable
    public String getAPaterno() {
        return APaterno;
    }

    @Nullable
    public String getAMaterno() {
        return AMaterno;
    }

    @Nullable
    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    @Nullable
    public String getCorreo() {
        return Correo;
    }

    public int getIdArea() {
        return IdArea;
    }
}
