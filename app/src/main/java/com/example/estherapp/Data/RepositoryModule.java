package com.example.estherapp.Data;

import android.app.Application;

import androidx.room.Room;

import com.example.estherapp.Data.ModuleUser.LocalUser.LocalUserDataSource;
import com.example.estherapp.Data.ModuleUser.LocalUser.UserDao;
import com.example.estherapp.Data.ModuleUser.RemoteUser.RemoteUserDataSource;
import com.example.estherapp.Data.ModuleUser.UserDataSource;
import com.example.estherapp.Util.AppExecutors;
import com.example.estherapp.Util.DiskIOThreadExecutor;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Provides;

@dagger.Module
abstract public class RepositoryModule {
    private static final int THREAD_COUNT = 3;

    @Singleton
    @Binds
    @Local
    abstract UserDataSource provideLocalDataSource(LocalUserDataSource dataSource);

    @Singleton
    @Binds
    @Remote
    abstract UserDataSource provideRemoteDataSource(RemoteUserDataSource dataSource);

    @Singleton
    @Provides
    static UserDao provideTaskDao(StructureDataBase db){
        return db.usuarioDao();
    }

    @Singleton
    @Provides
    static StructureDataBase provideDb(Application context) {

        return Room.databaseBuilder(context.getApplicationContext(),
                StructureDataBase.class, "nameDataBase.db")
                .fallbackToDestructiveMigration()
                .build();

    }

    @Singleton
    @Provides
    static AppExecutors provideAppExecutors() {
        return new AppExecutors(new DiskIOThreadExecutor(),
                Executors.newFixedThreadPool(THREAD_COUNT),
                new AppExecutors.MainThreadExecutor());
    }

}
