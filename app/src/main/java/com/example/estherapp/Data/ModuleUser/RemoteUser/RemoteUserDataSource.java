package com.example.estherapp.Data.ModuleUser.RemoteUser;

import androidx.annotation.NonNull;

import com.example.estherapp.Data.ModuleUser.UserDataSource;
import com.example.estherapp.Model.User;
import com.example.estherapp.Util.AppExecutors;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RemoteUserDataSource implements UserDataSource {

    private final AppExecutors mAppExecutors;

    @Inject
    public RemoteUserDataSource(@NonNull AppExecutors mAppExecutors) {
        this.mAppExecutors = mAppExecutors;
    }

    @Override
    public void saveUser(User user, response response) {

    }

    @Override
    public void getAllUser(selectRecords listenerSelect) {

    }

    @Override
    public void getLast(GetLast getLast) {

    }
}
