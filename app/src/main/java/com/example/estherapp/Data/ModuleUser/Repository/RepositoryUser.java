package com.example.estherapp.Data.ModuleUser.Repository;

import com.example.estherapp.Data.Local;
import com.example.estherapp.Data.ModuleUser.UserDataSource;
import com.example.estherapp.Data.Remote;
import com.example.estherapp.Model.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RepositoryUser implements UserDataSource {

    private final UserDataSource remoteDataSource;
    private final UserDataSource localDataSource;

    @Inject
    public RepositoryUser(@Remote UserDataSource remoteDataSource,
                          @Local UserDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public void saveUser(User user, final response response) {
        localDataSource.saveUser(user, new response() {
            @Override
            public void successful(long res) {
                response.successful(res);
            }

            @Override
            public void fail(String message) {
                response.fail(message);
            }
        });
    }

    @Override
    public void getAllUser(selectRecords listenerSelect) {
        localDataSource.getAllUser(new selectRecords() {
            @Override
            public void records(List<User> users) {
                listenerSelect.records(users);
            }

            @Override
            public void message(String message) {
                listenerSelect.message(message);
            }
        });
    }

    @Override
    public void getLast(GetLast getLast) {
        localDataSource.getLast(new GetLast() {
            @Override
            public void lastUser(User user) {
                getLast.lastUser(user);
            }
        });
    }
}
