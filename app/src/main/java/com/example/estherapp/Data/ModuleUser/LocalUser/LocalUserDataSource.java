package com.example.estherapp.Data.ModuleUser.LocalUser;


import com.example.estherapp.Data.ModuleUser.UserDataSource;
import com.example.estherapp.Model.User;
import com.example.estherapp.Util.AppExecutors;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LocalUserDataSource implements UserDataSource {

    private final UserDao dao;
    private final AppExecutors mAppExecutors;

    @Inject
    public LocalUserDataSource(UserDao dao, AppExecutors mAppExecutors) {
        this.dao = dao;
        this.mAppExecutors = mAppExecutors;
    }

    @Override
    public void saveUser(final User user, final response response) {
        //new Runnable() -- public void run()
        Runnable saveRunnable = () -> {
            long res = dao.inser(user);
            if (res > 0){
                response.successful(res);
            }else {
                response.fail("Fail");
            }
        };

        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public void getAllUser(selectRecords listenerSelect) {
        Runnable selectRunnable = new Runnable() {
            @Override
            public void run() {
                final List<User> users = dao.getAllUser();
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (!users.isEmpty()){
                            listenerSelect.records(users);
                        }else{
                            listenerSelect.message("Empty");
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(selectRunnable);
    }

    @Override
    public void getLast(GetLast getLast) {
        Runnable selectRunnable = new Runnable() {
            @Override
            public void run() {
                final User user = dao.getLast();
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        getLast.lastUser(user);
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(selectRunnable);
    }

}
