package com.example.estherapp.Data.ModuleUser;

import com.example.estherapp.Model.User;

import java.util.List;

public interface UserDataSource {

    interface response{
        void successful(long res);
        void fail(String message);
    }

    interface selectRecords{
        void records(List<User> users);
        void message(String message);
    }

    interface GetLast{
        void lastUser(User user);
    }

    void saveUser(User user, response response);
    void getAllUser(selectRecords listenerSelect);
    void getLast(GetLast getLast);

}
