package com.example.estherapp.Data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.estherapp.Data.ModuleUser.LocalUser.UserDao;
import com.example.estherapp.Model.User;

//entities nombres de la tablas de la base de datos
@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class StructureDataBase extends RoomDatabase {

    public abstract UserDao usuarioDao();

}
