package com.example.estherapp.Data.ModuleUser.LocalUser;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.estherapp.Model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long inser (User user);

    @Query("SELECT * FROM User")
    List<User> getAllUser();

    @Query("SELECT * FROM USER WHERE idUser = (SELECT MAX(idUser) FROM User)")
    User getLast();
}

